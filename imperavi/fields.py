# -*- coding: utf-8 -*-
from django.db.models import TextField
from django.conf import settings
from django.contrib.admin import widgets as admin_widgets
from django.forms.fields import CharField

from imperavi.widgets import RedactorEditor


class RedactorFormField(CharField):
    widget = RedactorEditor


class RedactorField(TextField):
    def formfield(self, **kwargs):
        defaults = {'max_length': self.max_length, 'widget': RedactorEditor}
        defaults.update(kwargs)
        return super(RedactorField, self).formfield(**defaults)
