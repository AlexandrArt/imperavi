# -*- coding: utf-8 -*-
from django import forms
from django.forms import widgets
from django.utils.safestring import mark_safe
try:
    from django.urls import reverse_lazy
except:
    from django.core.urlresolvers import reverse_lazy
from django.conf import settings

from imperavi.utils import json_dumps_lazy
from imperavi.default_settings import DEFAULT_REDACTOR_OPTIONS, REDACTOR_UPLOAD


class RedactorEditor(widgets.Textarea):
    @property
    def media(self):
        plugins = ['redactor/plugins/{0}.js'.format(plugin) for plugin in self.options.get('plugins')]
        return forms.Media(
            css={
                'all': (
                    'codemirror/lib/codemirror.css',
                    'redactor/css/redactor.min.css',
                    'redactor/css/django_admin.css',
                )
            },
            js=[
                'codemirror/lib/codemirror.js',
                'codemirror/mode/xml/xml.js',
                'codemirror/addon/edit/matchtags.js',
                'codemirror/addon/fold/xml-fold.js',

                'redactor/js/redactor.js',
                'redactor/langs/ru.js',
                'redactor/js/redactor.init.js',
            ] + plugins
        )

    def __init__(self, *args, **kwargs):
        self.options = DEFAULT_REDACTOR_OPTIONS.copy()
        self.options.update(getattr(settings, 'REDACTOR_OPTIONS', dict()))
        upload_to = getattr(settings, 'REDACTOR_UPLOAD', REDACTOR_UPLOAD)
        if self.options.get('fileUpload'):
            self.options['fileUpload'] = reverse_lazy('imperavi_upload_file', kwargs={'upload_to': REDACTOR_UPLOAD}),
        if self.options.get('imageUpload'):
            self.options['imageUpload'] = reverse_lazy('imperavi_upload_image', kwargs={'upload_to': REDACTOR_UPLOAD}),
        widget_attrs = {'class': 'redactor-box'}
        widget_attrs.update(kwargs.get('attrs', {}))
        widget_attrs['data-typograf'] = reverse_lazy('typograf')
        kwargs['attrs'] = widget_attrs
        super(RedactorEditor, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, renderer=None):
        """
        Must parse self.options with json_dumps on self.render.
        Because at some point Django calls RedactorEditor.__init__ before
        loading the urls, and it will break.
        """
        attrs['data-redactor-options'] = json_dumps_lazy(self.options)
        html = super(RedactorEditor, self).render(name, value, attrs, renderer=None)
        return mark_safe(html)