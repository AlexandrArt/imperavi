# -*- coding: utf-8 -*-
from django.utils.html import format_html, format_html_join
from django.conf import settings

try:
    from wagtail.core import __version__ as WAGTAIL_VERSION
    from wagtail.core import hooks
except:
    from wagtail.wagtailcore import __version__ as WAGTAIL_VERSION
    from wagtail.wagtailcore import hooks

version_numbers = WAGTAIL_VERSION.split('.')
if int(version_numbers[0]) == 1 and int(version_numbers[1]) < 7:    # if version < 1.7
    @hooks.register('insert_editor_js')
    def redactor_js():
        js = [
            '/static/codemirror/lib/codemirror.js',
            '/static/codemirror/mode/xml/xml.js',
            '/static/codemirror/addon/edit/matchtags.js',
            '/static/codemirror/addon/fold/xml-fold.js',
            '/static/redactor/js/redactor.js',
            '/static/redactor/langs/ru.js',
            '/static/redactor/js/redactor.init.js',
            '/static/redactor/plugins/codemirror.js',
        ]

        js_includes = '\n'.join(['\n<script type="text/javascript" src="%s"></script>' % filename for filename in js])

        return js_includes


    @hooks.register('insert_editor_css')
    def redactor_css():
        css = [
            '/static/codemirror/lib/codemirror.css',
            '/static/redactor/css/redactor.min.css',
            '/static/redactor/css/django_admin.css',
        ]

        css_includes = '\n'.join(['\n<link type="text/css" rel="stylesheet" href="%s">' % filename for filename in css])

        return css_includes
