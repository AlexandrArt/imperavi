# -*- coding: utf-8 -*-

REDACTOR_UPLOAD = 'uploads/'

FILE_UPLOAD_PERMISSIONS = 0o644

DEFAULT_REDACTOR_OPTIONS = {
    'lang': 'ru',
    'pastePlainText': False,
    'fileUpload': True,
    'imageUpload': True,
    'buttons': ['format', 'bold', 'italic', 'underline', 'deleted', 'lists', 'image', 'file', 'link', 'horizontalrule'],
    'pasteInlineTags': ['strong', 'u', 'em', 'del', 'code', 'span', 'ins', 'samp', 'kbd', 'sup', 'sub', 'mark', 'var', 'cite', 'small'],
    'formatting': ['p', 'h1', 'h2', 'h3'],
    'plugins': ['typograf', 'codemirror', 'table', 'fontcolor'],
    'codemirror': {
        'lineWrapping': True,
        'lineNumbers': True,
        'mode': 'text/html',
        'matchBrackets': True,
        'smartIndent': True,
        'matchTags': {
            'bothTags': True
        },
    }
}
