# -*- coding: utf-8 -*-
from django.conf.urls import url, include

import typograf.urls

from imperavi.views import RedactorUploadView
from imperavi.forms import FileForm, ImageForm


urlpatterns = [
    url(r'^upload/image/(?P<upload_to>.*)', RedactorUploadView.as_view(form_class=ImageForm), name='imperavi_upload_image'),
    url(r'^upload/file/(?P<upload_to>.*)', RedactorUploadView.as_view(form_class=FileForm), name='imperavi_upload_file'),
    url(r'^typograf/', include(typograf.urls)),
]
