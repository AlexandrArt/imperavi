(function ($) {
	$.Redactor.prototype.typograf = function () {
		return {
			langs: {
				en: {
					'typograf': 'Typograf',
					'error': 'Error!'
				},
				ru: {
					'typograf': 'Типограф',
					'error': 'Ошибка!'
				}
			},
			init: function () {
				var redactor = this;

				var button = redactor.button.addAfter('format', 'typograf', redactor.lang.get('typograf'));
				this.button.setIcon(button, '<i class="re-icon-bookmark"></i>');
				this.button.addCallback(button, redactor.typograf.process);
			},
			done: function () {
				var redactor = this;

				redactor.progress.hide();
				redactor.selection.restore();
			},
			insertResult: function (result) {
				var redactor = this;

				redactor.insert.set(result);
				redactor.typograf.done();
			},
			showError: function (errorText, errorCode) {
				var redactor = this;

				redactor.modal.load('error', redactor.lang.get('error'), 400);

				var $modal = redactor.modal.getModal();
				var modalBody = (errorCode !== undefined ? errorCode + ': ' : '') + errorText;

				$modal.text(modalBody);

				redactor.modal.show();
				redactor.typograf.done();
			},
			process: function () {
				var redactor = this;

				redactor.placeholder.hide();
				redactor.buffer.set();

				var $editor = redactor.core.editor();
				var editorHTML = $editor.html();

				var $textarea = redactor.core.textarea();
				var processingURL = $textarea.data('typograf');

				redactor.progress.show();

				$.ajax({
					url: processingURL,
					type: 'post',
					dataType: 'json',
					data: {text: editorHTML},
					success: function (result) {
						if (result && result.success === true) {
							redactor.typograf.insertResult(result.text);
						} else if (result && result.success === false) {
							redactor.typograf.showError(result.error);
						}
					},
					error: function (error) {
						redactor.typograf.showError(error.statusText, error.status);
					}
				});
			}
		};
	};
})(jQuery);
