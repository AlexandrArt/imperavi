/* global jQuery */
(function ($) {
	$(function () {
		var $document = $(document);

		var init_redactor = function () {
			var $redactors = $('textarea.redactor-box');
			if ($redactors.length) {
				$redactors.each(function () {
					var $redactor = $(this);

					if (!$redactor.is('.hook-done')) {
						$redactor.redactor($redactor.data('redactor-options'));
						$redactor.addClass('hook-done');
					}
				});
			}
		};

		$document.ready(init_redactor());
		$document.on('click', 'button[class*="icon-redactor"], button[class*="icon-plus"], a[class*="toggle"], a[class*="icon-plus"]', function () {
			init_redactor();
		});
	});
}(jQuery));
