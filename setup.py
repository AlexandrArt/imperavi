from setuptools import setup, find_packages

setup(
    name="imperavi-redactor",
    version="0.1.11",
    author="Alexandr Art",
    author_email="alexartemjev@gmail.com",
    packages=find_packages(),
    include_package_data=True,
    description="A django application that contains a class for admin interface to render a \
        text field as beautiful Imperavi WYSIWYG editor",
    long_description=open('README.rst', 'r').read(),
    license="MIT License",
    keywords="django imperavi",
    platforms=['any'],
    install_requires=[
        "python-typograf == 0.2.0",
    ],
    url="https://github.com/vasyabigi/django-imperavi",
)
